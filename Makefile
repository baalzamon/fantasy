# Configuration:
CC:=clang
PROJ_NAME:=fantasy

LIBS:=glfw3

COMMON_C_FLAGS:=$(shell pkg-config --cflags $(LIBS) 2>/dev/null) -std=c17 -pipe -fuse-ld=lld -D_DEFAULT_SOURCE=1
LDFILES:=$(shell pkg-config --libs $(LIBS) 2>/dev/null)

WARNS:=-Wall -pedantic -Wextra -Wshadow -Wunreachable-code -Wconversion -Wsign-conversion -Wformat -Wmissing-braces -Wparentheses -Wno-unused-command-line-argument

CFLAGS_DEBUG:=$(COMMON_C_FLAGS) $(WARNS) -UNDEBUG -DDEBUG -glldb -O0 -rdynamic -fno-omit-frame-pointer -DFY_USE_ASSERTS -fsanitize=undefined -static-libsan -static-libgcc
CFLAGS_RELEASE:=$(COMMON_C_FLAGS) $(WARNS) -DNDEBUG -UDEBUG -march=native -mtune=native -mavx2 -msse4.2 -fwrapv -g0 -s -Ofast -ffast-math -flto=full -fomit-frame-pointer

BUILD_DIR:=./bin
SRC_DIRS:=./src

SRCS:=$(shell find -O3 $(SRC_DIRS) -name '*.c')
ANALYZE_SRCS:=$(shell find -O3 $(SRC_DIRS) -name '*.c' -or -name '*.h')

OBJS:=$(SRCS:%=$(BUILD_DIR)/%.o)

DEPS:=$(OBJS:.o=.d)

INC_DIRS:=$(shell find $(SRC_DIRS) -type d)
CPPFLAGS:=$(addprefix -I,$(INC_DIRS)) -MMD -MP

.PHONY: debug release all analyze clean

debug: CFLAGS:=$(CFLAGS_DEBUG)
debug: all
	@./compile_shaders.sh -O0 -g

release: CFLAGS:=$(CFLAGS_RELEASE)
release: all
	llvm-strip -sx --strip-sections $(PROJ_NAME)
	@./compile_shaders.sh -O

all: clean $(BUILD_DIR)/$(PROJ_NAME)
	mv $(BUILD_DIR)/$(PROJ_NAME) .
	rm -rf $(BUILD_DIR)

$(BUILD_DIR)/$(PROJ_NAME): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) $(LDFILES) -o $@

# The "insecureAPI" check is often tells you to use non-portable functions, so turn it off.
# The valist check is buggy, so turn it off too.
analyze:
	clang-tidy $(ANALYZE_SRCS) \
		--checks="-clang-analyzer-security.insecureAPI.*,-clang-analyzer-valist.Uninitialized,-clang-diagnostic-incompatible-pointer-types-discards-qualifiers" \
		-- $(CPPFLAGS) $(CFLAGS_DEBUG)

$(BUILD_DIR)/%.c.o: %.c
	mkdir -p $(dir $@)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

clean:
	-rm -rf $(BUILD_DIR)

-include $(DEPS)
