#!/usr/bin/env sh
set -e

SHADER_COMPILE="glslc --target-env=vulkan1.2 $@"

for shader in *.glsl; do
	shader_compiled="$(basename "$shader" .glsl).spv"
	printf "Compiling %s.\n" "$shader"
	$SHADER_COMPILE "$shader" -o "$shader_compiled"
done
