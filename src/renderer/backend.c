#include "renderer.h"
#include <GLFW/glfw3.h>

#ifdef DEBUG
static const u32 wanted_layer_count = 1;
static const char *const wanted_vk_layers[1] = {"VK_LAYER_KHRONOS_validation"};
#endif

static const char **wanted_exts = 0;
static bool first_swapchain = true;

static const FYVertex triangle_vertices[3] = {
    {{0.0f, -1.0f}, {1.0f, 0.0f, 0.0f}},
    {{1.0f, 1.0f}, {0.0f, 1.0f, 0.0f}},
    {{-1.0f, 1.0f}, {0.0f, 0.0f, 1.0f}},
};

// Separate, static functions to simplify error paths.
#ifdef DEBUG
static VKAPI_ATTR VkBool32 VKAPI_CALL FYrenderer_callback_vk_debug(
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, VkDebugUtilsMessageTypeFlagsEXT messageType,
    const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData, void *pUserData);
#endif
static void FYrenderer_callback_fb_resize(GLFWwindow *win, int w, int h);
static void FYrenderer_callback_error(int e, const char *desc);
static FY_INLINE VkResult FYrenderer_enable_layers_and_extensions(void);
static FY_INLINE VkResult FYrenderer_instance_init(FYRenderer rend[static 1]);
static FY_INLINE VkResult FYrenderer_physical_device_init(FYRenderer rend[static 1]);
static FY_INLINE VkResult FYrenderer_physical_device_queues_init(FYRenderer rend[static 1]);
static FY_INLINE VkResult FYrenderer_swapchain_init(FYRenderer rend[static 1]);
static FY_INLINE VkResult FYshader_get(const char shader_path[restrict static 1], u32 *ret_bufptr[restrict static 1],
                                       size_t ret_bufsiz[restrict static 1]);
static FY_INLINE VkResult FYrenderer_shaders_init(FYRenderer rend[static 1]);
static FY_INLINE VkResult FYrenderer_render_pass_init(FYRenderer rend[static 1]);
static FY_INLINE VkResult FYrenderer_pipeline_init(FYRenderer rend[static 1]);
static FY_INLINE VkResult FYrenderer_framebuffer_init(FYRenderer rend[static 1]);
static FY_INLINE VkResult FYrenderer_command_pool_and_buf_init(FYRenderer rend[static 1]);
static FY_INLINE VkResult FYrenderer_sync_primitives_init(FYRenderer rend[static 1]);
static FY_INLINE VkResult FYrenderer_record_cmd_buf(FYRenderer rend[restrict static 1], VkCommandBuffer cmd_buf,
                                                    u32 img_index);
static FY_INLINE VkResult FYrenderer_recreate_swapchain(FYRenderer rend[static 1]);
static FY_INLINE void FYrenderer_set_resolution(FYRenderer rend[static 1], int w, int h);
static FY_INLINE VkResult FYrenderer_vertex_buffer_init(FYRenderer rend[static 1]);
static FY_INLINE u32 find_memory_type(FYRenderer rend[static 1], u32 type_filter, VkMemoryPropertyFlags properties);



VkResult FYrenderer_init(FYRenderer rend[static 1]) {
	VkResult e;

	glfwSetErrorCallback(FYrenderer_callback_error);

	if (!glfwInit()) {
		FY_FATAL("Failed loading GLFW.");
		e = VK_ERROR_INITIALIZATION_FAILED;
		goto init_fail_glfw;
	}

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_VISIBLE, GLFW_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
	glfwWindowHint(GLFW_DECORATED, GLFW_TRUE);
	glfwWindowHint(GLFW_FOCUSED, GLFW_TRUE);
	rend->win = glfwCreateWindow(800, 600, "fantasy", 0, 0);
	if (!rend->win) {
		FY_FATAL("Unable to create window.");
		e = VK_ERROR_INITIALIZATION_FAILED;
		goto init_fail_window;
	}
	FY_TRACE("Window created.");

	glfwSetWindowUserPointer(rend->win, rend);
	glfwSetFramebufferSizeCallback(rend->win, FYrenderer_callback_fb_resize);

	int glad_vk_ver = gladLoaderLoadVulkan(0, 0, 0);
	if (!glad_vk_ver) {
		FY_FATAL("Unable to load Vulkan.");
		e = VK_ERROR_INITIALIZATION_FAILED;
		goto init_fail_glad;
	}
	FY_TRACE("GLAD loaded Vulkan %d.%d.", GLAD_VERSION_MAJOR(glad_vk_ver), GLAD_VERSION_MINOR(glad_vk_ver));

	wanted_exts = vector_create();

	// Instance.
	e = FYrenderer_instance_init(rend);
	if (e != VK_SUCCESS)
		goto init_fail_instance;

	// Reload GLAD after instance is created.
	glad_vk_ver = gladLoaderLoadVulkan(rend->vk_instance, rend->vk_pdevice, 0);
	if (!glad_vk_ver) {
		FY_FATAL("Unable to reload Vulkan for instance.");
		e = VK_ERROR_INITIALIZATION_FAILED;
		goto init_fail_glad_instance;
	}

#ifdef DEBUG
	e = vkCreateDebugUtilsMessengerEXT(rend->vk_instance, &rend_info.debug_messenger_cinfo, 0,
	                                   &rend->debug_messenger);
	if (e != VK_SUCCESS) {
		FY_FATAL("Unable to create debug messenger.");
		goto init_fail_debug_messenger;
	}
	FY_DEBUG("Created debug messenger.");
#endif

	// Surface.
	e = glfwCreateWindowSurface(rend->vk_instance, rend->win, 0, &rend->vk_surface);
	if (e != VK_SUCCESS) {
		FY_FATAL("Unable to create Vulkan surface.");
		goto init_fail_surface;
	}
	FY_TRACE("Vulkan window surface created.");

	// Physical device.
	e = FYrenderer_physical_device_init(rend);
	if (e != VK_SUCCESS)
		goto init_fail_pdevice;

	// Reload GLAD after physical device is chosen.
	glad_vk_ver = gladLoaderLoadVulkan(rend->vk_instance, rend->vk_pdevice, 0);
	if (!glad_vk_ver) {
		FY_FATAL("Unable to reload Vulkan for physical device.");
		e = VK_ERROR_INITIALIZATION_FAILED;
		goto init_fail_glad_pdevice;
	}
	FY_TRACE("GLAD reloaded Vulkan %d.%d for physical device.", GLAD_VERSION_MAJOR(glad_vk_ver),
	         GLAD_VERSION_MINOR(glad_vk_ver));

	// Physical device queues.
	e = FYrenderer_physical_device_queues_init(rend);
	if (e != VK_SUCCESS) {
		FY_FATAL("Unable to select required physical device queues.");
		goto init_fail_pdevice_queues;
	}

	// Logical device.
	e = vkCreateDevice(rend->vk_pdevice, &rend_info.ldevice_cinfo, 0, &rend->vk_ldevice);
	if (e != VK_SUCCESS) {
		FY_FATAL("Unable to create logical device.");
		goto init_fail_ldevice;
	}
	FY_TRACE("Logical device created.");

	// Reload GLAD for the final time, now that we have a logical device.
	glad_vk_ver = gladLoaderLoadVulkan(rend->vk_instance, rend->vk_pdevice, rend->vk_ldevice);
	if (!glad_vk_ver) {
		FY_FATAL("Unable to reload Vulkan for logical device.");
		e = VK_ERROR_INITIALIZATION_FAILED;
		goto init_fail_glad_ldevice;
	}
	FY_TRACE("GLAD reloaded Vulkan %d.%d for logical device.", GLAD_VERSION_MAJOR(glad_vk_ver),
	         GLAD_VERSION_MINOR(glad_vk_ver));

	// Get the queues.
	vkGetDeviceQueue(rend->vk_ldevice, rend->q_graphics_index, 0, &rend->q_graphics);
	vkGetDeviceQueue(rend->vk_ldevice, rend->q_present_index, 0, &rend->q_present);

	e = FYrenderer_swapchain_init(rend);
	if (e != VK_SUCCESS)
		goto init_fail_swapchain;

	e = FYrenderer_shaders_init(rend);
	if (e != VK_SUCCESS)
		goto init_shaders_fail;

	e = FYrenderer_render_pass_init(rend);
	if (e != VK_SUCCESS)
		goto init_render_pass_fail;

	e = FYrenderer_pipeline_init(rend);
	if (e != VK_SUCCESS)
		goto init_pipeline_fail;

	e = FYrenderer_framebuffer_init(rend);
	if (e != VK_SUCCESS)
		goto init_framebuffer_fail;

	e = FYrenderer_command_pool_and_buf_init(rend);
	if (e != VK_SUCCESS)
		goto init_cmd_pool_and_buf_fail;

	e = FYrenderer_sync_primitives_init(rend);
	if (e != VK_SUCCESS)
		goto init_sync_primitives_fail;

	return VK_SUCCESS;

	// Easy failure return.
init_sync_primitives_fail:
	vkDestroyBuffer(rend->vk_ldevice, rend->vertex_buf, 0);
	vkFreeMemory(rend->vk_ldevice, rend->vertex_buf_mem, 0);
	vkDestroyCommandPool(rend->vk_ldevice, rend->cmd_pool, 0);
init_cmd_pool_and_buf_fail:
init_framebuffer_fail:
	vkDestroyPipeline(rend->vk_ldevice, rend->graphics_pipeline, 0);
	vkDestroyPipelineLayout(rend->vk_ldevice, rend->pipeline_layout, 0);
init_pipeline_fail:
	vkDestroyRenderPass(rend->vk_ldevice, rend->render_pass, 0);
init_render_pass_fail:
	vkDestroyShaderModule(rend->vk_ldevice, rend->vert_module, 0);
	vkDestroyShaderModule(rend->vk_ldevice, rend->frag_module, 0);
init_shaders_fail:
	for (u32 i = 0; i < rend->swapchain_img_count; ++i)
		vkDestroyImageView(rend->vk_ldevice, rend->swapchain_img_views[i], 0);
	free(rend->swapchain_img_views);
	free(rend->swapchain_imgs);
	vkDestroySwapchainKHR(rend->vk_ldevice, rend->vk_swapchain, 0);
init_fail_swapchain:
init_fail_glad_ldevice:
	vkDestroyDevice(rend->vk_ldevice, 0);
init_fail_ldevice:
init_fail_pdevice_queues:
init_fail_glad_pdevice:
init_fail_pdevice:
	vkDestroySurfaceKHR(rend->vk_instance, rend->vk_surface, 0);
init_fail_surface:
#ifdef DEBUG
	vkDestroyDebugUtilsMessengerEXT(rend->vk_instance, rend->debug_messenger, 0);
init_fail_debug_messenger:
#endif
init_fail_glad_instance:
	vkDestroyInstance(rend->vk_instance, 0);
init_fail_instance:
	vector_free(wanted_exts);
	if (glad_vk_ver)
		gladLoaderUnloadVulkan();
init_fail_glad:
	glfwDestroyWindow(rend->win);
init_fail_window:
	glfwTerminate();
init_fail_glfw:
	return e;
}

void FYrenderer_stop(FYRenderer rend[static 1]) {
	for (size_t i = 0; i < FY_FRAMES_IN_FLIGHT; ++i) {
		vkDestroySemaphore(rend->vk_ldevice, rend->render_finished_semaphore[i], 0);
		vkDestroySemaphore(rend->vk_ldevice, rend->img_available_semaphore[i], 0);
		vkDestroyFence(rend->vk_ldevice, rend->in_flight_fence[i], 0);
	}
	vkDestroyBuffer(rend->vk_ldevice, rend->vertex_buf, 0);
	vkFreeMemory(rend->vk_ldevice, rend->vertex_buf_mem, 0);
	vkDestroyCommandPool(rend->vk_ldevice, rend->cmd_pool, 0);
	for (u32 i = 0; i < rend->swapchain_img_count; ++i)
		vkDestroyFramebuffer(rend->vk_ldevice, rend->fbos[i], 0);
	free(rend->fbos);
	vkDestroyPipeline(rend->vk_ldevice, rend->graphics_pipeline, 0);
	vkDestroyPipelineLayout(rend->vk_ldevice, rend->pipeline_layout, 0);
	vkDestroyRenderPass(rend->vk_ldevice, rend->render_pass, 0);
	vkDestroyShaderModule(rend->vk_ldevice, rend->vert_module, 0);
	vkDestroyShaderModule(rend->vk_ldevice, rend->frag_module, 0);
	for (u32 i = 0; i < rend->swapchain_img_count; ++i) {
		vkDestroyImageView(rend->vk_ldevice, rend->swapchain_img_views[i], 0);
	}
	free(rend->swapchain_img_views);
	free(rend->swapchain_imgs);
	vkDestroySwapchainKHR(rend->vk_ldevice, rend->vk_swapchain, 0);
	vkDestroyDevice(rend->vk_ldevice, 0);
	vkDestroySurfaceKHR(rend->vk_instance, rend->vk_surface, 0);
#ifdef DEBUG
	vkDestroyDebugUtilsMessengerEXT(rend->vk_instance, rend->debug_messenger, 0);
#endif
	vkDestroyInstance(rend->vk_instance, 0);
	gladLoaderUnloadVulkan();
	vector_free(wanted_exts);
	glfwDestroyWindow(rend->win);
	glfwTerminate();
	FY_TRACE("Renderer stopped successfully.");
}

VkResult Fyrenderer_draw_frame(FYRenderer rend[static 1]) {
	static size_t current_frame = 0;

	// Wait for previous frame to finish.
	vkWaitForFences(rend->vk_ldevice, 1, &rend->in_flight_fence[current_frame], VK_TRUE, UINT64_MAX);

	if (rend->fb_resized) {
		rend->fb_resized = false;
		VkResult e = FYrenderer_recreate_swapchain(rend);
		if (e != VK_SUCCESS)
			return e;
	}

	u32 img_index = 0;
	VkResult e = vkAcquireNextImageKHR(rend->vk_ldevice, rend->vk_swapchain, UINT64_MAX,
	                                   rend->img_available_semaphore[current_frame], VK_NULL_HANDLE, &img_index);

	switch (e) {
	case VK_ERROR_OUT_OF_DATE_KHR:
		FYrenderer_recreate_swapchain(rend);
		return VK_SUCCESS;
	case VK_SUBOPTIMAL_KHR:
	case VK_SUCCESS:
		break;
	default:
		return e;
	}

	vkResetFences(rend->vk_ldevice, 1, &rend->in_flight_fence[current_frame]);

	vkResetCommandBuffer(rend->cmd_buf[current_frame], 0);
	rend_info.submit_info.pCommandBuffers = &rend->cmd_buf[current_frame];
	FYrenderer_record_cmd_buf(rend, rend->cmd_buf[current_frame], img_index);

	rend_info.submit_info.pWaitSemaphores = &rend->img_available_semaphore[current_frame];
	rend_info.submit_info.pSignalSemaphores = &rend->render_finished_semaphore[current_frame];

	e = vkQueueSubmit(rend->q_graphics, 1, &rend_info.submit_info, rend->in_flight_fence[current_frame]);
	if (e != VK_SUCCESS) {
		FY_ERROR("vkQueueSubmit failed with error %d.", e);
		return e;
	}

	rend_info.present_info.pImageIndices = &img_index;
	rend_info.present_info.pWaitSemaphores = &rend->render_finished_semaphore[current_frame];
	e = vkQueuePresentKHR(rend->q_present, &rend_info.present_info);

	switch (e) {
	case VK_SUBOPTIMAL_KHR:
	case VK_ERROR_OUT_OF_DATE_KHR: {
		VkResult e2 = FYrenderer_recreate_swapchain(rend);
		if (e2 != VK_SUCCESS)
			return e2;
	}
	case VK_SUCCESS:
		break;
	default:
		return e;
	}

	current_frame = (current_frame + 1) % FY_FRAMES_IN_FLIGHT;
	return VK_SUCCESS;
}



// Static functions.
#ifdef DEBUG
static VKAPI_ATTR VkBool32 VKAPI_CALL FYrenderer_callback_vk_debug(
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, VkDebugUtilsMessageTypeFlagsEXT messageType,
    const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData, void *pUserData) {
	FY_UNUSED(pUserData);
	FY_UNUSED(messageType);
	static const char *const fmt = "Vulkan: %s.";

	switch (messageSeverity) {
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
		FY_ERROR(fmt, pCallbackData->pMessage);
		break;
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
		FY_WARN(fmt, pCallbackData->pMessage);
		break;
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
		FY_DEBUG(fmt, pCallbackData->pMessage);
		break;
	default:
		break;
	}

	return VK_FALSE;
}
#endif

static void FYrenderer_callback_fb_resize(GLFWwindow *win, int w, int h) {
	FYRenderer *rend = (FYRenderer *)glfwGetWindowUserPointer(win);
	rend->fb_resized = true;
	FYrenderer_set_resolution(rend, w, h);
}

static void FYrenderer_callback_error(int e, const char *desc) { FY_ERROR("GLFW (error %d): %s", e, desc); }

static FY_INLINE VkResult FYrenderer_enable_layers_and_extensions(void) {
#ifdef DEBUG
	u32 available_layers_count = 0;
	vkEnumerateInstanceLayerProperties(&available_layers_count, 0);
	VkLayerProperties *available_layers = malloc(sizeof(VkLayerProperties[available_layers_count]));
	if (!available_layers)
		return VK_ERROR_OUT_OF_HOST_MEMORY;

	vkEnumerateInstanceLayerProperties(&available_layers_count, available_layers);
	for (u32 i = 0; i < wanted_layer_count; ++i) {
		bool found = false;
		for (u32 j = 0; j < available_layers_count; ++j) {
			if (!strcmp(wanted_vk_layers[i], available_layers[j].layerName)) {
				FY_DEBUG("Enabling validation layer: \"%s.\"", wanted_vk_layers[i]);
				found = true;
				break;
			}
		}

		if (!found) {
			FY_FATAL("Missing validation layer \"%s\" [%u].", wanted_vk_layers[i], i);
			free(available_layers);
			return VK_ERROR_LAYER_NOT_PRESENT;
		}
	}
	free(available_layers);

	rend_info.instance_cinfo.enabledLayerCount = wanted_layer_count;
	rend_info.instance_cinfo.ppEnabledLayerNames = wanted_vk_layers;
#endif

	u32 exts_count = 0;
	const char **exts = glfwGetRequiredInstanceExtensions(&exts_count);

	for (u32 i = 0; i < exts_count; ++i) {
		FY_TRACE("Enabling required extension %u: \"%s.\"", i, exts[i]);
		vector_add(&wanted_exts, const char *, exts[i]);
	}

#ifdef DEBUG
	// Enable debug messenger extension.
	vector_add(&wanted_exts, const char *, VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
	FY_DEBUG("Enabling \"" VK_EXT_DEBUG_UTILS_EXTENSION_NAME "\" extension.");
#endif

	rend_info.instance_cinfo.enabledExtensionCount = (u32)vector_size(wanted_exts);
	rend_info.instance_cinfo.ppEnabledExtensionNames = wanted_exts;
	return VK_SUCCESS;
}

static FY_INLINE VkResult FYrenderer_instance_init(FYRenderer rend[static 1]) {
	VkResult e = FYrenderer_enable_layers_and_extensions();
	if (e != VK_SUCCESS)
		return e;

#ifdef DEBUG
	rend_info.debug_messenger_cinfo.pfnUserCallback = FYrenderer_callback_vk_debug;
	rend_info.instance_cinfo.pNext = &rend_info.debug_messenger_cinfo;
#endif

	e = vkCreateInstance(&rend_info.instance_cinfo, 0, &rend->vk_instance);
	if (e != VK_SUCCESS) {
		FY_FATAL("Failed creating Vulkan instance.");
		return e;
	}

	FY_TRACE("Vulkan instance created.");
	return VK_SUCCESS;
}

static FY_INLINE VkResult FYrenderer_physical_device_init(FYRenderer rend[static 1]) {
	u32 dev_count = 0;
	vkEnumeratePhysicalDevices(rend->vk_instance, &dev_count, 0);
	VkPhysicalDevice *devs = malloc(sizeof(VkPhysicalDevice[dev_count]));
	if (!devs)
		return VK_ERROR_OUT_OF_HOST_MEMORY;
	vkEnumeratePhysicalDevices(rend->vk_instance, &dev_count, devs);

	u32 dev_ext_count = 0;
	vkEnumerateDeviceExtensionProperties(devs[0], 0, &dev_ext_count, 0);
	VkExtensionProperties *dev_exts = malloc(sizeof(VkExtensionProperties[dev_ext_count]));
	if (!dev_exts) {
		free(devs);
		return VK_ERROR_OUT_OF_HOST_MEMORY;
	}

	// Check for swapchain support.
	bool found = false;
	for (u32 i = 0; i < dev_ext_count; ++i) {
		if (!strcmp(VK_KHR_SWAPCHAIN_EXTENSION_NAME, dev_exts[i].extensionName)) {
			found = true;
			break;
		}
	}
	free(dev_exts);

	if (!found) {
		FY_FATAL("Missing required device extension \"" VK_KHR_SWAPCHAIN_EXTENSION_NAME ".\"");
		free(devs);
		return VK_ERROR_EXTENSION_NOT_PRESENT;
	}

	rend->vk_pdevice = devs[0];
	free(devs);

	VkPhysicalDeviceProperties pdevice_props;
	vkGetPhysicalDeviceProperties(rend->vk_pdevice, &pdevice_props);
	FY_DEBUG("\"%s\" Vulkan physical device chosen.", pdevice_props.deviceName);

	// TODO: Handle surface format and swapchain present mode properly.

	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(rend->vk_pdevice, rend->vk_surface, &rend->surface_capabilities);

	// Set initial framebuffer size.
	int w, h;
	glfwGetFramebufferSize(rend->win, &w, &h);
	FYrenderer_set_resolution(rend, w, h);

	rend_info.swapchain_cinfo.preTransform = rend->surface_capabilities.currentTransform;
	rend_info.swapchain_cinfo.surface = rend->vk_surface;
	// Set to FY_FRAMES_IN_FLIGHT + 1, assuming minImageCount is 2, which it probably is.
	rend_info.swapchain_cinfo.minImageCount = rend->surface_capabilities.minImageCount + FY_FRAMES_IN_FLIGHT - 1;
	if (rend->surface_capabilities.maxImageCount > 0 &&
	    rend_info.swapchain_cinfo.minImageCount > rend->surface_capabilities.maxImageCount)
		rend_info.swapchain_cinfo.minImageCount = rend->surface_capabilities.maxImageCount;

	return VK_SUCCESS;
}

static FY_INLINE VkResult FYrenderer_physical_device_queues_init(FYRenderer rend[static 1]) {
	u32 queue_family_count = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(rend->vk_pdevice, &queue_family_count, 0);

	VkQueueFamilyProperties *queue_families = malloc(sizeof(VkQueueFamilyProperties[queue_family_count]));
	if (!queue_families)
		return VK_ERROR_OUT_OF_HOST_MEMORY;

	vkGetPhysicalDeviceQueueFamilyProperties(rend->vk_pdevice, &queue_family_count, queue_families);
	bool g_found = false, p_found = false;
	for (u32 i = 0; i < queue_family_count; ++i) {
		if (queue_families[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
			rend->q_graphics_index = i;
			g_found = true;
		}

		VkBool32 present_supported = false;
		vkGetPhysicalDeviceSurfaceSupportKHR(rend->vk_pdevice, i, rend->vk_surface, &present_supported);
		if (present_supported) {
			rend->q_present_index = i;
			p_found = true;
		}
	}
	free(queue_families);

	if (!g_found) {
		FY_FATAL("Device chosen does not have a graphics queue.");
		return VK_ERROR_FEATURE_NOT_PRESENT;
	}

	if (!p_found) {
		FY_FATAL("Device chosen does not have a present queue.");
		return VK_ERROR_FEATURE_NOT_PRESENT;
	}

	// Set them up as separate queues first.
	rend->q_priorities[0] = 1.0f;
	rend->q_priorities[1] = 1.0f;
	rend_info.q_cinfo[0].queueFamilyIndex = rend->q_graphics_index;
	rend_info.q_cinfo[0].pQueuePriorities = &rend->q_priorities[0];
	rend_info.q_cinfo[1].queueFamilyIndex = rend->q_present_index;
	rend_info.q_cinfo[1].pQueuePriorities = &rend->q_priorities[1];
	rend_info.cmd_pool_cinfo.queueFamilyIndex = rend->q_graphics_index;

	// If they are the same queue, we must not initialize them as separate.
	if (rend->q_graphics_index == rend->q_present_index) {
		rend_info.q_cinfo[0].queueCount = 2;
		rend_info.ldevice_cinfo.queueCreateInfoCount = 1;
		rend_info.swapchain_cinfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	} else {
		rend_info.swapchain_cinfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		rend_info.swapchain_cinfo.queueFamilyIndexCount = 2;
		rend_info.swapchain_cinfo.pQueueFamilyIndices = rend->q_indices_as_array;
	}

	FY_TRACE("Physical device queues set up.");
	return VK_SUCCESS;
}

static FY_INLINE VkResult FYrenderer_swapchain_init(FYRenderer rend[static 1]) {
	VkResult e = vkCreateSwapchainKHR(rend->vk_ldevice, &rend_info.swapchain_cinfo, 0, &rend->vk_swapchain);
	if (e != VK_SUCCESS) {
		FY_FATAL("Failed creating swapchain.");
		return e;
	}

	vkGetSwapchainImagesKHR(rend->vk_ldevice, rend->vk_swapchain, &rend->swapchain_img_count, 0);
	rend->swapchain_imgs = malloc(sizeof(VkImage[rend->swapchain_img_count]));
	if (!rend->swapchain_imgs) {
		e = VK_ERROR_OUT_OF_HOST_MEMORY;
		goto imgs_alloc_fail;
	}

	vkGetSwapchainImagesKHR(rend->vk_ldevice, rend->vk_swapchain, &rend->swapchain_img_count, rend->swapchain_imgs);
	if (first_swapchain)
		FY_TRACE("Using %u swapchain images.", rend->swapchain_img_count);

	rend->swapchain_img_views = malloc(sizeof(VkImageView[rend->swapchain_img_count]));
	if (!rend->swapchain_img_views) {
		e = VK_ERROR_OUT_OF_HOST_MEMORY;
		goto views_alloc_fail;
	}

	for (u32 i = 0; i < rend->swapchain_img_count; ++i) {
		rend_info.swapchain_img_view_cinfo.image = rend->swapchain_imgs[i];
		e = vkCreateImageView(rend->vk_ldevice, &rend_info.swapchain_img_view_cinfo, 0,
		                      &rend->swapchain_img_views[i]);

		if (e != VK_SUCCESS) {
			FY_FATAL("Failed creating image view %u.", i + 1);
			for (u32 j = 0; j < i; ++j)
				vkDestroyImageView(rend->vk_ldevice, rend->swapchain_img_views[j], 0);
			goto views_fail;
		}
	}

	if (first_swapchain)
		FY_TRACE("Swapchain created.");
	return VK_SUCCESS;

views_fail:
	free(rend->swapchain_img_views);
views_alloc_fail:
	free(rend->swapchain_imgs);
imgs_alloc_fail:
	vkDestroySwapchainKHR(rend->vk_ldevice, rend->vk_swapchain, 0);
	return e;
}

static FY_INLINE VkResult FYshader_get(const char shader_path[restrict static 1], u32 *ret_bufptr[restrict static 1],
                                       size_t ret_bufsiz[restrict static 1]) {
	if (!ret_bufptr)
		return VK_ERROR_UNKNOWN;

	FY_TRACE("Getting shader \"%s\" from filesystem.", shader_path);
	FILE *f = fopen(shader_path, "rb");
	if (!f)
		return VK_ERROR_UNKNOWN;

	size_t len = FYfile_size(f);

	// Make sure buf is aligned sufficiently (to 4 bytes) for Vulkan.
	// (And aligned_alloc() requires that size is a multiple of alignment.)
	u32 *buf = aligned_alloc(4, len + (4 - len % 4) % 4);
	if (!buf) {
		fclose(f);
		return VK_ERROR_OUT_OF_HOST_MEMORY;
	}

	size_t num_read = fread(buf, 1, len, f);

	// Read may not have been successful.
	if (num_read != len) {
		if (ferror(f)) {
			fclose(f);
			free(buf);
			return VK_ERROR_UNKNOWN;
		}
		FY_TRACE("Shader file shorter than reported earlier.");
	}
	fclose(f);

	*ret_bufptr = buf;
	*ret_bufsiz = len;
	return VK_SUCCESS;
}

static FY_INLINE VkResult FYrenderer_shaders_init(FYRenderer rend[static 1]) {
	u32 *vertbuf = 0;
	u32 *fragbuf = 0;
	size_t vertlen = 0;
	size_t fraglen = 0;
	VkResult e = VK_SUCCESS;

	e = FYshader_get("triangle.vert.spv", &vertbuf, &vertlen);
	if (e != VK_SUCCESS) {
		FY_FATAL("Failed reading vertex shader.");
		goto get_vert_fail;
	}

	e = FYshader_get("triangle.frag.spv", &fragbuf, &fraglen);
	if (e != VK_SUCCESS) {
		FY_FATAL("Failed reading fragment shader.");
		goto get_frag_fail;
	}

	rend_info.shader_module_vertex_cinfo.codeSize = vertlen;
	rend_info.shader_module_vertex_cinfo.pCode = vertbuf;
	rend_info.shader_module_fragment_cinfo.codeSize = fraglen;
	rend_info.shader_module_fragment_cinfo.pCode = fragbuf;

	e = vkCreateShaderModule(rend->vk_ldevice, &rend_info.shader_module_vertex_cinfo, 0, &rend->vert_module);
	if (e != VK_SUCCESS) {
		FY_FATAL("Failed creating vertex shader module.");
		goto vert_module_fail;
	}

	e = vkCreateShaderModule(rend->vk_ldevice, &rend_info.shader_module_fragment_cinfo, 0, &rend->frag_module);
	if (e != VK_SUCCESS) {
		FY_FATAL("Failed creating fragment shader module.");
		goto frag_module_fail;
	}

	rend_info.shader_stages_cinfo[0].module = rend->vert_module;
	rend_info.shader_stages_cinfo[1].module = rend->frag_module;

	FY_TRACE("Shaders initialized.");
	return VK_SUCCESS;

frag_module_fail:
	vkDestroyShaderModule(rend->vk_ldevice, rend->vert_module, 0);
vert_module_fail:
	free(fragbuf);
get_frag_fail:
	free(vertbuf);
get_vert_fail:
	return e;
}

static FY_INLINE VkResult FYrenderer_render_pass_init(FYRenderer rend[static 1]) {
	rend->viewport.maxDepth = 1.0f;
	rend_info.pipeline_viewport_state_cinfo.pViewports = &rend->viewport;
	rend_info.pipeline_viewport_state_cinfo.pScissors = &rend->scissor;

	VkResult e = vkCreateRenderPass(rend->vk_ldevice, &rend_info.pass_cinfo, 0, &rend->render_pass);
	if (e != VK_SUCCESS)
		FY_FATAL("Failed creating render pass.");
	FY_TRACE("Render pass created.");
	return e;
}

static FY_INLINE VkResult FYrenderer_pipeline_init(FYRenderer rend[static 1]) {
	VkResult e =
	    vkCreatePipelineLayout(rend->vk_ldevice, &rend_info.pipeline_layout_cinfo, 0, &rend->pipeline_layout);
	if (e != VK_SUCCESS) {
		FY_FATAL("Failed creating graphics pipeline layout.");
		return e;
	}

	rend_info.graphics_pipeline_cinfo.layout = rend->pipeline_layout;
	rend_info.graphics_pipeline_cinfo.renderPass = rend->render_pass;
	e = vkCreateGraphicsPipelines(rend->vk_ldevice, VK_NULL_HANDLE, 1, &rend_info.graphics_pipeline_cinfo, 0,
	                              &rend->graphics_pipeline);
	if (e != VK_SUCCESS) {
		FY_FATAL("Failed creating graphics pipeline.");
		vkDestroyPipelineLayout(rend->vk_ldevice, rend->pipeline_layout, 0);
		return e;
	}

	FY_TRACE("Graphics pipeline created.");
	return VK_SUCCESS;
}

static FY_INLINE VkResult FYrenderer_framebuffer_init(FYRenderer rend[static 1]) {
	rend->fbos = malloc(sizeof(VkFramebuffer[rend->swapchain_img_count]));
	if (!rend->fbos)
		return VK_ERROR_OUT_OF_HOST_MEMORY;

	rend_info.fbo_cinfo.renderPass = rend->render_pass;

	// Make all framebuffers needed.
	for (u32 i = 0; i < rend->swapchain_img_count; ++i) {
		rend_info.fbo_cinfo.pAttachments = &rend->swapchain_img_views[i];

		VkResult e = vkCreateFramebuffer(rend->vk_ldevice, &rend_info.fbo_cinfo, 0, &rend->fbos[i]);
		if (e != VK_SUCCESS) {
			FY_FATAL("Failed creating framebuffer %u.", i + 1);
			// Unwind on error.
			for (u32 j = 0; j < i; ++j)
				vkDestroyFramebuffer(rend->vk_ldevice, rend->fbos[j], 0);
			free(rend->fbos);
			return e;
		}
	}

	if (first_swapchain)
		FY_TRACE("Framebuffers created.");
	first_swapchain = false;
	return VK_SUCCESS;
}

static FY_INLINE VkResult FYrenderer_command_pool_and_buf_init(FYRenderer rend[static 1]) {

	// Command pool.
	VkResult e = vkCreateCommandPool(rend->vk_ldevice, &rend_info.cmd_pool_cinfo, 0, &rend->cmd_pool);
	if (e != VK_SUCCESS) {
		FY_FATAL("Failed creating command pool.");
		return e;
	}
	FY_TRACE("Command pool created.");

	rend_info.cmd_buf_allocate_info.commandPool = rend->cmd_pool;

	e = FYrenderer_vertex_buffer_init(rend);
	if (e != VK_SUCCESS) {
		vkDestroyCommandPool(rend->vk_ldevice, rend->cmd_pool, 0);
		return e;
	}

	// Command buffers. Automatically destroyed with the pool.
	e = vkAllocateCommandBuffers(rend->vk_ldevice, &rend_info.cmd_buf_allocate_info, rend->cmd_buf);
	if (e != VK_SUCCESS) {
		FY_FATAL("Failed to allocate command buffer.");
		vkDestroyBuffer(rend->vk_ldevice, rend->vertex_buf, 0);
		vkFreeMemory(rend->vk_ldevice, rend->vertex_buf_mem, 0);
		vkDestroyCommandPool(rend->vk_ldevice, rend->cmd_pool, 0);
		return e;
	}
	FY_TRACE("Command buffer created.");

	rend_info.pass_begin_info.renderPass = rend->render_pass;

	return VK_SUCCESS;
}

static FY_INLINE VkResult FYrenderer_sync_primitives_init(FYRenderer rend[static 1]) {
	VkResult e;
	for (size_t i = 0; i < FY_FRAMES_IN_FLIGHT; ++i) {
		e = vkCreateFence(rend->vk_ldevice, &rend_info.fence_cinfo, 0, &rend->in_flight_fence[i]);
		if (e != VK_SUCCESS) {
			for (size_t j = 0; j < i; ++j) {
				vkDestroySemaphore(rend->vk_ldevice, rend->render_finished_semaphore[j], 0);
				vkDestroySemaphore(rend->vk_ldevice, rend->img_available_semaphore[j], 0);
				vkDestroyFence(rend->vk_ldevice, rend->in_flight_fence[j], 0);
			}
			return e;
		}
		e = vkCreateSemaphore(rend->vk_ldevice, &rend_info.semaphore_cinfo, 0,
		                      &rend->img_available_semaphore[i]);
		if (e != VK_SUCCESS) {
			vkDestroyFence(rend->vk_ldevice, rend->in_flight_fence[i], 0);
			for (size_t j = 0; j < i; ++j) {
				vkDestroySemaphore(rend->vk_ldevice, rend->render_finished_semaphore[j], 0);
				vkDestroySemaphore(rend->vk_ldevice, rend->img_available_semaphore[j], 0);
				vkDestroyFence(rend->vk_ldevice, rend->in_flight_fence[j], 0);
			}
			return e;
		}
		e = vkCreateSemaphore(rend->vk_ldevice, &rend_info.semaphore_cinfo, 0,
		                      &rend->render_finished_semaphore[i]);
		if (e != VK_SUCCESS) {
			vkDestroyFence(rend->vk_ldevice, rend->in_flight_fence[i], 0);
			vkDestroySemaphore(rend->vk_ldevice, rend->img_available_semaphore[i], 0);
			for (size_t j = 0; j < i; ++j) {
				vkDestroySemaphore(rend->vk_ldevice, rend->render_finished_semaphore[j], 0);
				vkDestroySemaphore(rend->vk_ldevice, rend->img_available_semaphore[j], 0);
				vkDestroyFence(rend->vk_ldevice, rend->in_flight_fence[j], 0);
			}
			return e;
		}
	}

	FY_TRACE("Synchronization primitives created.");

	rend_info.submit_info.pWaitDstStageMask = &rend_info.submit_dst_stage_mask;

	rend_info.present_info.pSwapchains = &rend->vk_swapchain;

	return VK_SUCCESS;
}

static FY_INLINE VkResult FYrenderer_record_cmd_buf(FYRenderer rend[restrict static 1], VkCommandBuffer cmd_buf,
                                                    u32 img_index) {
	VkResult e = vkBeginCommandBuffer(cmd_buf, &rend_info.cmd_buf_begin_info);
	if (e != VK_SUCCESS) {
		FY_ERROR("vkBeginCommandBuffer() failed with error %d.", e);
		return e;
	}

	FY_ASSERT(img_index < rend->swapchain_img_count);
	rend_info.pass_begin_info.framebuffer = rend->fbos[img_index];

	vkCmdBeginRenderPass(cmd_buf, &rend_info.pass_begin_info, VK_SUBPASS_CONTENTS_INLINE);
	vkCmdBindPipeline(cmd_buf, VK_PIPELINE_BIND_POINT_GRAPHICS, rend->graphics_pipeline);
	vkCmdSetViewport(cmd_buf, 0, 1, &rend->viewport);
	vkCmdSetScissor(cmd_buf, 0, 1, &rend->scissor);
	VkDeviceSize vertex_buf_offset = 0;
	vkCmdBindVertexBuffers(cmd_buf, 0, 1, &rend->vertex_buf, &vertex_buf_offset);
	vkCmdDraw(cmd_buf, 3, 1, 0, 0);
	vkCmdEndRenderPass(cmd_buf);

	e = vkEndCommandBuffer(cmd_buf);
	if (e != VK_SUCCESS) {
		FY_ERROR("vkEndCommandBuffer() failed with error %d.", e);
		return e;
	}

	return VK_SUCCESS;
}

// Assumes the resolution is already correct.
static FY_INLINE VkResult FYrenderer_recreate_swapchain(FYRenderer rend[static 1]) {
	// Checks for minimized window.
	while (!(rend->scissor.extent.width | rend->scissor.extent.height))
		glfwWaitEvents();

	vkDeviceWaitIdle(rend->vk_ldevice);

	FY_TRACE("Recreating swapchain.");

	// Destroy old swapchain.
	for (u32 i = 0; i < rend->swapchain_img_count; ++i)
		vkDestroyFramebuffer(rend->vk_ldevice, rend->fbos[i], 0);
	free(rend->fbos);
	for (u32 i = 0; i < rend->swapchain_img_count; ++i) {
		vkDestroyImageView(rend->vk_ldevice, rend->swapchain_img_views[i], 0);
	}
	free(rend->swapchain_img_views);
	free(rend->swapchain_imgs);
	vkDestroySwapchainKHR(rend->vk_ldevice, rend->vk_swapchain, 0);

	// Make a new one.
	VkResult e = FYrenderer_swapchain_init(rend);
	if (e != VK_SUCCESS)
		return e;
	e = FYrenderer_framebuffer_init(rend);
	if (e != VK_SUCCESS)
		return e;

	return VK_SUCCESS;
}

// Does not recreate swapchain, but must be called before making a new one.
static FY_INLINE void FYrenderer_set_resolution(FYRenderer rend[static 1], int w, int h) {
	FY_TRACE("Setting resolution to (%d, %d).", w, h);
	rend->viewport.width = (float)w;
	rend->viewport.height = (float)h;
	rend_info.swapchain_cinfo.imageExtent = (VkExtent2D){(u32)w, (u32)h};
	if (rend_info.swapchain_cinfo.imageExtent.width < rend->surface_capabilities.minImageExtent.width)
		rend_info.swapchain_cinfo.imageExtent.width = rend->surface_capabilities.minImageExtent.width;
	else if (rend_info.swapchain_cinfo.imageExtent.width > rend->surface_capabilities.maxImageExtent.width)
		rend_info.swapchain_cinfo.imageExtent.width = rend->surface_capabilities.maxImageExtent.width;
	if (rend_info.swapchain_cinfo.imageExtent.height < rend->surface_capabilities.minImageExtent.height)
		rend_info.swapchain_cinfo.imageExtent.height = rend->surface_capabilities.minImageExtent.height;
	else if (rend_info.swapchain_cinfo.imageExtent.height > rend->surface_capabilities.maxImageExtent.height)
		rend_info.swapchain_cinfo.imageExtent.height = rend->surface_capabilities.maxImageExtent.height;
	rend_info.pass_begin_info.renderArea.extent = rend_info.swapchain_cinfo.imageExtent;
	rend->scissor.extent = rend_info.swapchain_cinfo.imageExtent;
	rend_info.fbo_cinfo.width = rend_info.swapchain_cinfo.imageExtent.width;
	rend_info.fbo_cinfo.height = rend_info.swapchain_cinfo.imageExtent.height;
	rend_info.pass_begin_info.renderArea.extent = rend_info.swapchain_cinfo.imageExtent;
}

static FY_INLINE VkResult FYrenderer_vertex_buffer_init(FYRenderer rend[static 1]) {
	VkResult e = vkCreateBuffer(rend->vk_ldevice, &rend_info.vertex_buf_cinfo, 0, &rend->vertex_buf);
	if (e != VK_SUCCESS) {
		FY_FATAL("Failed to create vertex buffer.");
		return e;
	}

	VkMemoryRequirements mem_requirements;
	vkGetBufferMemoryRequirements(rend->vk_ldevice, rend->vertex_buf, &mem_requirements);

	VkMemoryAllocateInfo mem_alloc_info = {
	    .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
	    .allocationSize = mem_requirements.size,
	    .memoryTypeIndex =
		find_memory_type(rend, mem_requirements.memoryTypeBits,
	                         VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT)};

	e = vkAllocateMemory(rend->vk_ldevice, &mem_alloc_info, 0, &rend->vertex_buf_mem);
	if (e != VK_SUCCESS) {
		FY_FATAL("Failed allocating memory for vertex buffer.");
		vkDestroyBuffer(rend->vk_ldevice, rend->vertex_buf, 0);
		return e;
	}

	vkBindBufferMemory(rend->vk_ldevice, rend->vertex_buf, rend->vertex_buf_mem, 0);

	// Map and memcpy() to GPU memory. (This is awesome!)
	void *mapped_vertex_buf_mem;
	vkMapMemory(rend->vk_ldevice, rend->vertex_buf_mem, 0, rend_info.vertex_buf_cinfo.size, 0,
	            &mapped_vertex_buf_mem);
	memcpy(mapped_vertex_buf_mem, triangle_vertices, rend_info.vertex_buf_cinfo.size);
	vkUnmapMemory(rend->vk_ldevice, rend->vertex_buf_mem);

	return VK_SUCCESS;
}

static FY_INLINE u32 find_memory_type(FYRenderer rend[static 1], u32 type_filter, VkMemoryPropertyFlags properties) {
	VkMemoryRequirements mem_requirements;
	vkGetBufferMemoryRequirements(rend->vk_ldevice, rend->vertex_buf, &mem_requirements);
	VkPhysicalDeviceMemoryProperties mem_properties;
	vkGetPhysicalDeviceMemoryProperties(rend->vk_pdevice, &mem_properties);

	for (u32 i = 0; i < mem_properties.memoryTypeCount; ++i) {
		if ((type_filter & (1 << i)) &&
		    (mem_properties.memoryTypes[i].propertyFlags & properties) == properties) {
			return i;
		}
	}

	FY_ASSERT_MSG(0, "No suitable memory types.");
}
