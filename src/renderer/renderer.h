#ifndef FY_RENDERER_H_
#define FY_RENDERER_H_

#include <cglm/types.h>
#include <stdbool.h>
#include <string.h>

#include "glad/vulkan.h"
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <cglm/cglm.h>

#include "../defines.h"
#include "../core/log.h"
#include "../core/vec.h"
#include "../core/io.h"

#define FY_FRAMES_IN_FLIGHT 2

typedef struct FYVertex FYVertex;
typedef struct FYRenderer FYRenderer;
typedef struct FYRendererInfo FYRendererInfo;

struct FYRenderer {
	GLFWwindow *win;
	bool fb_resized;

#ifdef DEBUG
	VkDebugUtilsMessengerEXT debug_messenger;
#endif

	// Vulkan.
	VkPhysicalDeviceFeatures vk_pdevice_features;
	VkInstance vk_instance;
	VkPhysicalDevice vk_pdevice;
	VkDevice vk_ldevice;
	VkSurfaceKHR vk_surface;
	VkSwapchainKHR vk_swapchain;
	VkSurfaceCapabilitiesKHR surface_capabilities;

	u32 swapchain_img_count;
	VkImage *swapchain_imgs;
	VkImageView *swapchain_img_views;
	VkFramebuffer *fbos;

	// Queues.
	VkQueue q_graphics;
	VkQueue q_present;
	float q_priorities[2];
	union {
		u32 q_indices_as_array[2];
		struct {
			u32 q_graphics_index;
			u32 q_present_index;
		};
	};

	// Rendering setup.
	VkShaderModule vert_module;
	VkShaderModule frag_module;
	VkViewport viewport;
	VkRect2D scissor;
	VkRenderPass render_pass;
	VkPipelineLayout pipeline_layout;
	VkPipeline graphics_pipeline;
	VkCommandPool cmd_pool;
	VkCommandBuffer cmd_buf[FY_FRAMES_IN_FLIGHT];
	VkBuffer vertex_buf;
	VkDeviceMemory vertex_buf_mem;

	// Synchronization primitives.
	VkSemaphore img_available_semaphore[FY_FRAMES_IN_FLIGHT];
	VkSemaphore render_finished_semaphore[FY_FRAMES_IN_FLIGHT];
	VkFence in_flight_fence[FY_FRAMES_IN_FLIGHT];
};

struct FYRendererInfo {
	VkApplicationInfo app_info;
	VkInstanceCreateInfo instance_cinfo;
	VkDeviceQueueCreateInfo q_cinfo[2];
	VkDeviceCreateInfo ldevice_cinfo;
	VkSwapchainCreateInfoKHR swapchain_cinfo;
	VkImageViewCreateInfo swapchain_img_view_cinfo;
	VkShaderModuleCreateInfo shader_module_vertex_cinfo;
	VkShaderModuleCreateInfo shader_module_fragment_cinfo;
	VkPipelineShaderStageCreateInfo shader_stages_cinfo[2];
	VkDynamicState pipeline_dynamic_states[2];
	VkPipelineDynamicStateCreateInfo pipeline_dynamic_states_cinfo;
	VkPipelineVertexInputStateCreateInfo vertex_input_state_cinfo;
	VkPipelineInputAssemblyStateCreateInfo pipeline_assembly_input_state_cinfo;
	VkPipelineViewportStateCreateInfo pipeline_viewport_state_cinfo;
	VkPipelineRasterizationStateCreateInfo pipeline_rasterization_state_cinfo;
	VkPipelineMultisampleStateCreateInfo pipeline_msaa_state_cinfo;
	VkPipelineColorBlendAttachmentState pipeline_color_blend_attachment_state;
	VkPipelineColorBlendStateCreateInfo pipeline_color_blend_cinfo;
	VkPipelineLayoutCreateInfo pipeline_layout_cinfo;
	VkAttachmentDescription pass_color_attachment;
	VkAttachmentReference pass_color_attachment_ref;
	VkSubpassDescription subpass_desc;
	VkRenderPassCreateInfo pass_cinfo;
	VkGraphicsPipelineCreateInfo graphics_pipeline_cinfo;
	VkFramebufferCreateInfo fbo_cinfo;
#ifdef DEBUG
	VkDebugUtilsMessengerCreateInfoEXT debug_messenger_cinfo;
#endif
	VkCommandPoolCreateInfo cmd_pool_cinfo;
	VkCommandBufferAllocateInfo cmd_buf_allocate_info;
	VkCommandBufferBeginInfo cmd_buf_begin_info;
	VkClearValue clear_color;
	VkRenderPassBeginInfo pass_begin_info;
	VkSemaphoreCreateInfo semaphore_cinfo;
	VkFenceCreateInfo fence_cinfo;
	VkSubmitInfo submit_info;
	VkPipelineStageFlagBits submit_dst_stage_mask;
	VkSubpassDependency subpass_dep;
	VkPresentInfoKHR present_info;
	VkVertexInputBindingDescription vertex_input_binding_desc;
	VkVertexInputAttributeDescription vertex_input_attrib_descs[2];
	VkBufferCreateInfo vertex_buf_cinfo;
};

struct FYVertex {
	vec2 position;
	vec3 color;
};

extern FYRendererInfo rend_info;

FY_API VkResult FYrenderer_init(FYRenderer rend[static 1]);
FY_API void FYrenderer_stop(FYRenderer rend[static 1]);
FY_API VkResult Fyrenderer_draw_frame(FYRenderer rend[static 1]);

#endif /* FY_RENDERER_H_ */
