#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "core/assertion.h"
#include "core/io.h"
#include "core/log.h"

#include "renderer/renderer.h"

static FYRenderer fy_rend = {0};

int main(void) {
	// Set relative zero time for logs.
	clock_gettime(CLOCK_REALTIME, &time_at_start);

	if (FYrenderer_init(&fy_rend) != VK_SUCCESS)
		return EXIT_FAILURE;

	while (!glfwWindowShouldClose(fy_rend.win)) {
		glfwPollEvents();
		if (Fyrenderer_draw_frame(&fy_rend) != VK_SUCCESS)
			break;
	}

	vkDeviceWaitIdle(fy_rend.vk_ldevice);
	FYrenderer_stop(&fy_rend);
	return EXIT_SUCCESS;
}
