#ifndef FY_DEFINES_H_
#define FY_DEFINES_H_

// Naming conventions:
// 	Macros and defines:	FY_SHOUTY_CASE
// 	Functions:		FY_snake_case()
// 	Enums/structs:		FYUpperCamelCase
// 	Enum members:		FY_SHOUTY_CASE
// 	struct members:		snake_case
// 	variables:		fy_snake_case

#include <stdint.h>

#define FY_API extern
#define FY_INLINE __always_inline
#define FY_UNUSED(var) ((void)(var))

typedef int8_t s8;
typedef uint8_t u8;

typedef int16_t s16;
typedef uint16_t u16;

typedef int32_t s32;
typedef uint32_t u32;

typedef int64_t s64;
typedef uint64_t u64;

typedef unsigned char uchar;
typedef unsigned short ushort;
typedef unsigned int uint;
typedef unsigned long ulong;

#endif /* FY_DEFINES_H_ */
