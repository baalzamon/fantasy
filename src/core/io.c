#include "io.h"

size_t FYfile_size(FILE f[static 1]) {
	int res = fseek(f, 0, SEEK_END);
	if (res == -1)
		return 0;
	long sz = ftell(f);
	rewind(f);
	if (sz == -1)
		return 0;
	return (size_t)sz;
}

size_t FYfile_readf(FILE f[restrict static 1], size_t buf_len, const char buf[restrict buf_len]) {
	if (!buf_len)
		return 0;

	clearerr(f);
	size_t num_read = fread((void *)buf, 1, buf_len, f);

	// Read may not have been successful.
	if (num_read != buf_len)
		if (ferror(f))
			return 0;

	return num_read;
}

size_t FYfile_read(const char file_name[restrict static 1], size_t buf_len, char buf[restrict buf_len]) {
	FILE *f = fopen(file_name, "r");
	if (!f)
		return 0;

	size_t r = FYfile_readf(f, buf_len, buf);
	fclose(f);

	return r;
}

char *FYfile_read_alloc(const char file_name[static 1]) {
	FILE *f = fopen(file_name, "r");
	if (!f)
		return 0;

	size_t sz = FYfile_size(f);
	if (!sz) {
		fclose(f);
		return 0;
	}

	char *buf = malloc(sz + 1);
	if (!buf) {
		fclose(f);
		return 0;
	}

	size_t r = FYfile_readf(f, sz, buf);
	fclose(f);

	if (!r) {
		free(buf);
		return 0;
	}

	buf[r] = (char)0;
	return buf;
}

size_t FYfile_writef(FILE f[restrict static 1], size_t len, const char msg[restrict len]) {
	if (!len)
		return 0;

	clearerr(f);
	size_t num_written = fwrite(msg, 1, len, f);

	// Write may not have been successful.
	if (num_written != len)
		if (ferror(f))
			return 0;

	return num_written;
}

size_t FYfile_write(const char file_name[restrict static 1], const char file_mode[restrict static 1], size_t len,
                    const char msg[restrict len]) {
	FILE *f = fopen(file_name, file_mode);
	if (!f)
		return 0;

	size_t r = FYfile_writef(f, len, msg);
	fclose(f);

	return r;
}
