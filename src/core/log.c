#include "log.h"

#ifdef DEBUG
uint fy_log_level = FY_LOG_LEVEL_TRACE;
#else
uint fy_log_level = FY_LOG_LEVEL_WARN;
#endif

struct timespec time_at_start = {0};

static FY_INLINE void timespec_subtract(struct timespec lhstime[restrict static 1],
                                        const struct timespec rhstime[restrict static 1]) {
	bool underflowed;
	lhstime->tv_nsec -= rhstime->tv_nsec;
	underflowed = lhstime->tv_nsec < 0L;
	lhstime->tv_sec -= rhstime->tv_sec;
	if (underflowed) {
		--lhstime->tv_sec;
		lhstime->tv_nsec += 1000000000L;
	}
}

void FYlog(const char func_name[restrict static 1], uint level, const char fmt[restrict static 1], ...) {
	if (level > fy_log_level)
		return;

	static const char *const log_color[] = {
	    [FY_LOG_LEVEL_FATAL] = "\033[31m", [FY_LOG_LEVEL_ERROR] = "\033[31m", [FY_LOG_LEVEL_WARN] = "\033[34m",
	    [FY_LOG_LEVEL_INFO] = "\033[33m",  [FY_LOG_LEVEL_DEBUG] = "\033[32m", [FY_LOG_LEVEL_TRACE] = "\033[90m"};
	static const char *const log_head[] = {
	    [FY_LOG_LEVEL_FATAL] = "[FATAL] ", [FY_LOG_LEVEL_ERROR] = "[ERROR] ", [FY_LOG_LEVEL_WARN] = "[WARN]  ",
	    [FY_LOG_LEVEL_INFO] = "[INFO]  ",  [FY_LOG_LEVEL_DEBUG] = "[DEBUG] ", [FY_LOG_LEVEL_TRACE] = "[TRACE] "};

	va_list va_args;

	// Use realtime and subtract from starting time to get total time elapsed globally.
	struct timespec log_time;
	clock_gettime(CLOCK_REALTIME, &log_time);
	timespec_subtract(&log_time, &time_at_start);
	__time_t usec = log_time.tv_nsec / 1000L;

	fprintf(stderr, "\033[1m%s[%ld.%.6ld] %s%s()\033[0m%s ", log_color[level], log_time.tv_sec, usec,
	        log_head[level], func_name, log_color[level]);
	va_start(va_args, fmt);
	vfprintf(stderr, fmt, va_args);
	va_end(va_args);
	fputs("\033[0m\n", stderr);
}
