#ifndef FY_CORE_LOG_H_
#define FY_CORE_LOG_H_

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>

#include "../defines.h"
#include "assertion.h"

enum FYLogLevel {
	FY_LOG_LEVEL_FATAL = 0,
	FY_LOG_LEVEL_ERROR,
	FY_LOG_LEVEL_WARN,
	FY_LOG_LEVEL_INFO,
	FY_LOG_LEVEL_DEBUG,
	FY_LOG_LEVEL_TRACE,
	FY_LOG_LEVELS_COUNT
};

// Prints everything below, including its own level.
extern uint fy_log_level;

// Set this with clock_gettime(CLOCK_REALTIME, &time_at_start) before any logs
// for the timestamps to work properly.
extern struct timespec time_at_start;

FY_API void FYlog(const char func_name[restrict static 1], uint level, const char fmt[restrict static 1], ...);

#define FY_FATAL(...) FYlog(__func__, FY_LOG_LEVEL_FATAL, __VA_ARGS__)
#define FY_ERROR(...) FYlog(__func__, FY_LOG_LEVEL_ERROR, __VA_ARGS__)
#define FY_WARN(...) FYlog(__func__, FY_LOG_LEVEL_WARN, __VA_ARGS__)

#ifdef DEBUG
#define FY_INFO(...) FYlog(__func__, FY_LOG_LEVEL_INFO, __VA_ARGS__)
#define FY_DEBUG(...) FYlog(__func__, FY_LOG_LEVEL_DEBUG, __VA_ARGS__)
#define FY_TRACE(...) FYlog(__func__, FY_LOG_LEVEL_TRACE, __VA_ARGS__)
#else
#define FY_INFO(...)
#define FY_DEBUG(...)
#define FY_TRACE(...)
#endif

#endif /* FY_CORE_LOG_H_ */
