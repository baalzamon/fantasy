#ifndef FY_CORE_ASSERTION_H_
#define FY_CORE_ASSERTION_H_

#include <stdio.h>
#include <stdlib.h>

#include "defines.h"

#ifdef FY_USE_ASSERTS
#define FY_ASSERT(expr)                                                                               \
	do {                                                                                          \
		if (!(expr)) {                                                                        \
			fprintf(stderr, "%s:%d: Assertion failure: %s\n", __FILE__, __LINE__, #expr); \
			__builtin_trap();                                                             \
		}                                                                                     \
	} while (0)

#define FY_ASSERT_MSG(expr, msg)                                                                                      \
	do {                                                                                                          \
		if (!(expr)) {                                                                                        \
			fprintf(stderr, "%s:%d: Assertion failure: %s, message: \"%s\"\n", __FILE__, __LINE__, #expr, \
			        (msg));                                                                               \
			__builtin_trap();                                                                             \
		}                                                                                                     \
	} while (0)
#else
#define FY_ASSERT(expr)
#define FY_ASSERT_MSG(expr, msg)
#endif

#endif /* FY_CORE_ASSERTION_H_ */
