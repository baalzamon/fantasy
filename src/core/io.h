#ifndef FY_CORE_IO_H_
#define FY_CORE_IO_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../defines.h"

FY_API size_t FYfile_size(FILE f[static 1]);

// Read from file into a buffer. Provided FILE must have read access.
// Length is in bytes. Provided buffer len must not be zero.
FY_API size_t FYfile_readf(FILE f[restrict static 1], size_t buf_len, const char buf[restrict buf_len]);
FY_API size_t FYfile_read(const char file_name[restrict static 1], size_t buf_len, char buf[restrict buf_len]);
FY_API char *FYfile_read_alloc(const char file_name[static 1]);

// Write to a file from a buffer. FILE must be opened with either write or append mode.
// Length is in bytes. Provided buffer len must not be zero.
FY_API size_t FYfile_writef(FILE f[restrict static 1], size_t len, const char msg[restrict len]);
FY_API size_t FYfile_write(const char file_name[restrict static 1], const char file_mode[restrict static 1], size_t len,
                           const char msg[restrict len]);

#endif /* FY_CORE_IO_H_ */
